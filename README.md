# ifml-moddle

Read and write IFML diagram files in NodeJS and the browser.

__ifml-moddle__ uses the [IFML meta-model](http://www.omg.org/spec/IFML/) to validate the input and produce correct IFML XML.


## Usage

Get the library via [npm package](https://www.npmjs.org/package/ifml-moddle). Consume it in NodeJS, via UMD or bundle it for the browser using your favorite module bundler.

```javascript
const moddle = new IfmlModdle();

const xmlStr =
  '<?xml version="1.0" encoding="UTF-8"?>' +
  '<xmi:XMI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ifml="http://www.omg.org/spec/IFML/20140301" xmlns:ifmldi="http://www.omg.org/spec/IFML/20130218/IFML-DI" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
  '<ifml:iFMLModel>' +
  '<ifml:interactionFlowModel id="_myModel" />' +
  '</ifml:iFMLModel>' +
  '<ifmldi:iFMLDiagram modelElement="_myModel" />' +
  '</xmi:XMI>';


const {
  rootElement: definitions
} = await moddle.fromXML(xmlStr);

// update id attribute
definitions.set('id', 'NEW ID');

// add a root element
const ifmlViewContainer = moddle.create('ifml:ViewContainer', {
  id: 'MyContainer'
});
var modelElements = definitions.get('ifmlModel')[0].get('interactionFlowModel').get(
  'interactionFlowModelElements');
modelElements.push(ifmlViewContainer);

moddle.toXML(definitions).then(function (xml) {
  console.log(xml);
})
```


## Resources

* [Issues](https://gitlab.com/metaaid/ifml-io/ifml-moddle/-/issues)
* [Examples](https://gitlab.com/metaaid/ifml-io/ifml-moddle/-/tree/master/test/spec/xml)

## Building the Project

Execute the test via

```
npm test
```

Perform a complete build of the application via

```
npm run all
```


## Related

The library is built based on [bpmn-moddle](https://github.com/bpmn-io/bpmn-moddle) and on top of [moddle](https://github.com/bpmn-io/moddle) and [moddle-xml](https://github.com/bpmn-io/moddle-xml).


## License

Use under the terms of the [MIT license](http://opensource.org/licenses/MIT).
