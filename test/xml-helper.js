import {
  readFile
} from './helper.js';

export function fromFile(moddle, file) {
  return fromFilePart(moddle, file, 'ifml:interactionFlowModelElements');
}

export function fromFilePart(moddle, file, type) {
  var fileContents = readFile(file);

  return moddle.fromXML(fileContents, type);
}

export function fromValidFile(moddle, file) {
  var fileContents = readFile(file);

  return moddle.fromXML(fileContents, 'ifml:interactionFlowModelElements');
}

export function toXML(element, opts) {
  return element.$model.toXML(element, opts);
}

export function validate(xml) {

  return new Promise(function(resolve, reject) {

    if (!xml) {
      return reject(new Error('XML is not defined'));
    }

    SchemaValidator.validateXML(xml, BPMN_XSD, function(err, result) {

      if (err) {
        return reject(err);
      }

      try {
        expect(result.valid).to.be.true;
      } catch (err) {
        return reject(err);
      }

      return resolve();
    });
  });
}