const { expect } = require('chai');


describe('ifml-moddle', function() {

  it('should expose CJS bundle', function() {
    const IfmlModdle = require('../../dist/index.cjs');

    expect(new IfmlModdle()).to.exist;
  });


  it('should expose UMD bundle', function() {
    const IfmlModdle = require('../../dist/ifml-moddle.umd.prod.cjs');

    expect(new IfmlModdle()).to.exist;
  });

});