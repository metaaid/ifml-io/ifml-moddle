import expect from '../expect.js';

import {
  createModdle
} from '../helper.js';


describe('ifml-moddle', function() {

  var moddle = createModdle();


  describe('parsing', function() {

    it('should publish type', function() {
      // when
      var type = moddle.getType('ifml:ViewContainer');

      // then
      expect(type).to.exist;
      expect(type.$descriptor).to.exist;
    });


    it('should redefine property', function() {

      // when
      var type = moddle.getType('ifmldi:IFMLNode');

      // then
      expect(type).to.exist;

      var descriptor = type.$descriptor;

      expect(descriptor).to.exist;
      expect(
        descriptor.propertiesByName['di:modelElement']
      ).to.eql(
        descriptor.propertiesByName['ifmldi:modelElement']
      );
    });

  });


  describe('creation', function() {

    it('should create ViewComponent', function() {
      var sequenceFlow = moddle.create('ifml:ViewComponent');

      expect(sequenceFlow.$type).to.eql('ifml:ViewComponent');
    });


    it('should create Definitions', function() {
      var definitions = moddle.create('xmi:XMI');

      expect(definitions.$type).to.eql('xmi:XMI');
    });


    it('should create Process', function() {
      var process = moddle.create('ifml:ViewContainer');

      expect(process.$type).to.eql('ifml:ViewContainer');
      expect(process.$instanceOf('ifml:ViewElement')).to.be.true;
    });


    it('should create DataFlow', function() {
      var subProcess = moddle.create('ifml:DataFlow');

      expect(subProcess.$type).to.eql('ifml:DataFlow');
      expect(subProcess.$instanceOf('ifml:InteractionFlow')).to.be.true;
    });


    describe('defaults', function() {

      it('should init Parameter', function() {
        var gateway = moddle.create('ifml:Parameter');

        expect(gateway.direction).to.eql('undefined');
      });
    });

  });


  describe('property access', function() {

    describe('singleton properties', function() {

      it('should set attribute', function() {

        // given
        var selectionField = moddle.create('ifml:SelectionField');

        // assume
        expect(selectionField.get('isMultiSelection')).not.to.exist;

        // when
        selectionField.set('isMultiSelection', true);

        // then
        expect(selectionField).to.jsonEqual({
          $type: 'ifml:SelectionField',
          isMultiSelection: true
        });
      });


      it('should set attribute (ns)', function() {

        // given
        var process = moddle.create('ifml:SelectionField');

        // when
        process.set('ifml:isMultiSelection', true);

        // then
        expect(process).to.jsonEqual({
          $type: 'ifml:SelectionField',
          isMultiSelection: true
        });
      });


      it('should set id attribute', function() {

        // given
        var definitions = moddle.create('xmi:XMI');

        // when
        definitions.set('id', 10);

        // then
        expect(definitions).to.jsonEqual({
          $type: 'xmi:XMI',
          id: 10
        });
      });
    });


    describe('builder', function() {

      it('should create simple hierarchy', function() {

        // given
        var definitions = moddle.create('xmi:XMI');
        var rootModel = moddle.create('ifml:IFMLModel');
        var interactionModel = moddle.create('ifml:InteractionFlowModel');
        var modelElements = interactionModel.get('interactionFlowModelElements');

        var viewContainer = moddle.create('ifml:ViewContainer');
        var viewContainerElements = viewContainer.get('viewElements');
        var viewComponent1 = moddle.create('ifml:ViewComponent');
        var viewComponent2 = moddle.create('ifml:ViewComponent');

        // when
        // definitions.push(rootModel);
        definitions.set('ownedElement', [rootModel]);
        rootModel.set('interactionFlowModel', [interactionModel]);
        interactionModel.set('interactionFlowModel', modelElements);
        modelElements.push(viewContainer);

        viewContainerElements.push(viewComponent1);
        viewContainerElements.push(viewComponent2);

        // then
        expect(viewContainerElements).to.eql([ viewComponent1, viewComponent2 ]);

        expect(definitions).to.jsonEqual({
          $type: 'xmi:XMI',
          ownedElement: [{
            $type: 'ifml:IFMLModel',
            interactionFlowModel:[{
              $type: 'ifml:InteractionFlowModel',
              interactionFlowModelElements: [{
                $type: 'ifml:ViewContainer',
                viewElements: [
                  { $type: 'ifml:ViewComponent' },
                  { $type: 'ifml:ViewComponent' }
                ]
              }]
            }]
          }]
        });
      });

    });

  });

});