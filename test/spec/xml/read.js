import expect from '../../expect.js';

import {
  createModdle,
  readFile
} from '../../helper.js';


describe('ifml-moddle - read', function() {

  var todoJson = JSON.parse(readFile("test/fixtures/json/model/todo.json"));
  todoJson["relPath"] = './todo.xmi';

  var moddle = createModdle([todoJson]);

  function read(xml, root, opts) {
    return moddle.fromXML(xml, root, opts);
  }

  function fromFile(file, root, opts) {
    var contents = readFile(file);
    return read(contents, root, opts);
  }

  describe('should load domain model', function() {
    it('should read classifier', async function() {
      // given
    
      // when
      // var { todo } = await fromFile('test/fixtures/ifml/todo.xmi');
      var { rootElement } = await fromFile('test/fixtures/ifml/classifier.ifml');
    
      // then
      expect(rootElement.ownedElement[0]).to.exist;
      expect(rootElement.ownedElement[0].domainModel).to.exist;
      expect(rootElement.ownedElement[0].domainModel).to.exist;
      expect(rootElement.ownedElement[0].domainModel.domainElements[0]).to.exist;
      expect(rootElement.ownedElement[0].domainModel.domainElements[0].$id).to.eq("myConcept");
      expect(rootElement.ownedElement[0].domainModel.domainElements[0].$type).to.eq("ifml:UMLDomainConcept");
      expect(rootElement.ownedElement[0].domainModel.domainElements[0].classifier).to.exist;
      expect(rootElement.ownedElement[0].domainModel.domainElements[0].classifier.name).to.eq("todo:TodoItem");
    });
  });


  describe('should import types', function() {

    describe('ifml', function() {

      it('read empty ifml', async function() {

        // given

        // when
        var { rootElement } = await fromFile('test/fixtures/ifml/empty.ifml');

        var expected = {
          "$type": "xmi:XMI",
          "$id": undefined,
          "ownedElement": [
            {
              "$type": "ifml:IFMLModel",
              "$id": undefined,
              "interactionFlowModel": {
                "$id": "_myModel",
                "$type": "ifml:InteractionFlowModel",
              }
            },
            {
              "$type": "ifmldi:IFMLDiagram",
              "$id": undefined,
              // "_myModel": "ifmldi:IFMLDiagram",
            }
          ]
        };

        // then
        expect(rootElement).to.jsonEqual(expected);
      });
  });
  });

});
