import fs from 'fs';
import expect from '../../expect.js';

import {
  assign
} from 'min-dash';

import {
  createModdle
} from '../../helper.js';


export function readFile(filename) {
  return fs.readFileSync(filename, { encoding: 'UTF-8' });
}

function xmiWrap(model, rootOptions){
  var root = model.create("xmi:XMI", rootOptions);
  var model = model.create("ifml:IFMLModel", rootOptions);
  var ownedElement = root.get("ownedElement");
  ownedElement.push(model);
  return root;
}

function xmiCreateRoot(model, firstChildsName, options = {}, rootOptions = {}){
  var dcRoot = model.create(firstChildsName, options);
  return xmiWrap(model, dcRoot, rootOptions);
}


describe('ifml-moddle - write', function() {

  var todoJson = JSON.parse(readFile("test/fixtures/json/model/todo.json"));
  todoJson["relPath"] = './todo.xmi';

  var moddle = createModdle([todoJson]);


  function write(element, options) {

    // skip preamble for tests
    options = assign({ preamble: false }, options);

    return moddle.toXML(element, options);
  }


  // describe('should export types', function() {

    describe('ifml', function() {

      it('classifier', async function() {

        // given
        var root =  xmiWrap(moddle);
        root.ownedElement[0].domainModel = moddle.create('ifml:DomainModel');
        root.ownedElement[0].domainModel.domainElements = [moddle.create('ifml:UMLDomainConcept')];
        root.ownedElement[0].domainModel.domainElements[0].classifier = moddle.create('todo:TodoItem').$descriptor;

        var expectedXML = `<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:ifml="http://www.omg.org/spec/IFML/20140301"><ifml:IFMLModel><domainModel xmi:type="ifml:DomainModel"><domainElements xmi:type="ifml:UMLDomainConcept"><classifier xmi:type="uml:Classifier" href="./todo.xmi#TodoItem" /></domainElements></domainModel></ifml:IFMLModel></xmi:XMI>`;        

        // when
        var { xml } = await write(root);

        // then
        expect(xml).to.eql(expectedXML);
      });
    });
});
