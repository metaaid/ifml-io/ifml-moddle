import {
  assign
} from 'min-dash';

import IfmlModdle from './ifml-moddle.js';

import DcPackage from '../resources/ifml/json/dc.json' assert { type: "json" };
import DiPackage from '../resources/ifml/json/di.json' assert { type: "json" };
import BiocPackage from '../resources/bpmn-io/json/bioc.json' assert { type: "json" };

import UMLPackage from '../resources/ifml/json/uml.json' assert { type: "json" };
import PrimitiveTypes from '../resources/ifml/json/primitivetypes.json' assert { type: "json" };
import IfmlPackage from '../resources/ifml/json/ifml.json' assert { type: "json" };
import IfmlDiPackage from '../resources/ifml/json/ifmldi.json' assert { type: "json" };
import XMIPackageLink from '../resources/ifml/json/xmi.json' assert { type: "json" };

var packages = {
  dc: DcPackage,
  di: DiPackage,
  bioc: BiocPackage,
  uml: UMLPackage,
  pt: PrimitiveTypes,
  ifml: IfmlPackage,
  ifmldi: IfmlDiPackage,
  xmi: XMIPackageLink,
};

export default function (additionalPackages, options) {
  var pks = assign({}, packages, additionalPackages);

  return new IfmlModdle(pks, options);
}